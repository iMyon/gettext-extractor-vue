const extractVue2Snippets = require('./extractVue2Snippets');
const extractVue3Snippets = require('./extractVue3Snippets');
const extractJSSnippets = require('./extractJSSnippets');

const extractSnippetsFactory = ({
  vue2TemplateCompiler = null,
  vue3TemplateCompiler = null,
} = {}) => {
  if (vue2TemplateCompiler && vue3TemplateCompiler) {
    throw new Error(
      'Please specify just vue2TemplateCompiler _or_ vue3TemplateCompiler as an option. These are mutually exclusive.'
    );
  }

  if (!vue2TemplateCompiler && !vue3TemplateCompiler) {
    throw new Error(
      'Please provide a template compiler via the vue2TemplateCompiler _or_ vue3TemplateCompiler option.'
    );
  }

  if (vue2TemplateCompiler) {
    return (file) => {
      if (file.endsWith('vue')) {
        return extractVue2Snippets(file, vue2TemplateCompiler);
      }
      return extractJSSnippets(file);
    };
  }

  return (file) => {
    if (file.endsWith('vue')) {
      return extractVue3Snippets(file, vue3TemplateCompiler);
    }
    return extractJSSnippets(file);
  };
};

module.exports = extractSnippetsFactory;

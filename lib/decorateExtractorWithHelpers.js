const decorateExtractorWithHelpers = (extractor) => {
  extractor.addMessageTransformFunction = (fn) => {
    if (extractor.$overwrittenBuilder) {
      return;
    }
    const TransformLineCatalogBuilder = require('./transformLineCatalogBuilder');
    extractor.builder = new TransformLineCatalogBuilder(extractor.builder, fn);

    extractor.$overwrittenBuilder = true;
  };

  return extractor;
};

module.exports = decorateExtractorWithHelpers;

const glob = require('glob');
const extractSnippetsFactory = require('./extractSnippetsFactory');

const decorateJSParserWithVueSupport = (jsParser, options) => {
  const originalParseString = jsParser.parseString;

  const extractSnippets = extractSnippetsFactory(options);

  jsParser.parseString = function (string, fileName, options = {}) {
    return Promise.resolve(originalParseString.call(this, string, fileName, options));
  };

  jsParser.parseFile = async function (fileName, options = {}) {
    const snippets = await extractSnippets(fileName);

    snippets.forEach((snippet) => {
      originalParseString.call(
        this,
        snippet.code,
        fileName,
        Object.assign({}, options, {
          lineNumberStart: snippet.line,
        })
      );
    });

    return jsParser;
  };

  async function parseFile(files, options, i) {
    await jsParser.parseFile(files[i], options);
    if (i > 0) {
      return parseFile(files, options, i - 1);
    }
  }

  jsParser.parseFilesGlob = async function (pattern, globOptions, options) {
    const files = glob.sync(pattern, globOptions).sort();

    await parseFile(files, options, files.length - 1);

    return jsParser;
  };

  return jsParser;
};

module.exports = decorateJSParserWithVueSupport;

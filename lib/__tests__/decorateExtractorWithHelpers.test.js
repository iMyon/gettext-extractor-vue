const test = require('ava');

const { decorateExtractorWithHelpers } = require('../../index');

const { GettextExtractor, JsExtractors } = require('gettext-extractor');

const initExtractor = () => {
  return decorateExtractorWithHelpers(new GettextExtractor());
};

test('addMessageTransformFunction: should apply transform function to extractor.addMessage', (t) => {
  const extractor = initExtractor();

  extractor.addMessageTransformFunction(() => 'FOO');

  extractor.addMessage({
    text: '\nTest\n',
    textPlural: '\nPlural\nspans\n  multiple lines.\n',
  });

  t.deepEqual(extractor.getMessages(), [
    {
      text: 'FOO',
      textPlural: 'FOO',
      context: null,
      references: [],
      comments: [],
    },
  ]);
});

test('addMessageTransformFunction: should apply transform function to parser.parseString', (t) => {
  const extractor = initExtractor();

  extractor.addMessageTransformFunction(() => 'FOO');

  const jsParser = extractor.createJsParser([
    JsExtractors.callExpression('__', {
      arguments: {
        text: 0,
        textPlural: 1,
      },
    }),
  ]);

  jsParser.parseString('__(`\n Test\r\n message\n`, ` Plural`)', 'a.js');

  t.deepEqual(extractor.getMessages(), [
    {
      text: 'FOO',
      textPlural: 'FOO',
      context: null,
      references: ['a.js:1'],
      comments: [],
    },
  ]);
});

test('addMessageTransformFunction: should throw error if string has been parsed before activating', (t) => {
  const extractor = initExtractor();

  extractor.addMessage({ text: 'Test' });
  t.throws(
    () => {
      extractor.addMessageTransformFunction(() => 'FOO');
    },
    {
      instanceOf: Error,
      message: 'Make sure you run `addMessageTransformFunction` before extracting any strings',
    }
  );
});

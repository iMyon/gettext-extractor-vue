const test = require('ava');
const path = require('path');
const glob = require('glob');

const extractSnippetsFactory = require('../extractSnippetsFactory');

const root = path.resolve(__dirname + '/__fixtures__/');

test('extractSnippetsFactory: should throw error if no compiler options are provided', (t) => {
  t.throws(
    () => {
      extractSnippetsFactory();
    },
    {
      instanceOf: Error,
      message:
        'Please provide a template compiler via the vue2TemplateCompiler _or_ vue3TemplateCompiler option.',
    }
  );
});

test('extractSnippetsFactory: should throw error if both compiler options are provided', (t) => {
  t.throws(
    () => {
      extractSnippetsFactory({
        vue2TemplateCompiler: require('vue-template-compiler'),
        vue3TemplateCompiler: require('vue-template-compiler'),
      });
    },
    {
      instanceOf: Error,
      message:
        'Please specify just vue2TemplateCompiler _or_ vue3TemplateCompiler as an option. These are mutually exclusive.',
    }
  );
});

const cleanResult = (snippets) =>
  snippets.map(({ filename, ...rest }) => ({ filename: path.basename(filename), ...rest }));

const extractSnippetsVue2 = extractSnippetsFactory({
  vue2TemplateCompiler: require('vue-template-compiler'),
});
const extractSnippetsVue3 = extractSnippetsFactory({
  vue3TemplateCompiler: require('@vue/compiler-sfc'),
});

const fixtures = glob.sync('*.{js,vue}', {
  cwd: root,
  absolute: true,
});

fixtures.forEach((file) => {
  test(`vue@2 (vue-template-compiler): should parse ${path.basename(
    file
  )} correctly`, async (t) => {
    const snippets = await extractSnippetsVue2(file);

    t.snapshot(cleanResult(snippets));
  });

  test(`vue@3 (@vue/compiler-sfc): should parse ${path.basename(file)} correctly`, async (t) => {
    const snippets = await extractSnippetsVue3(file);

    t.snapshot(cleanResult(snippets));
  });
});

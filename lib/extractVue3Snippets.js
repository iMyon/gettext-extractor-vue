const fs = require('fs');

const extractVue3Snippets = (filename, compiler) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, 'utf8', (err, file) => {
      if (err) {
        return reject(err);
      }

      const { descriptor } = compiler.parse(file, { pad: 'line', filename });

      const snippets = [];

      if (descriptor.template && descriptor.template.content) {
        const compiled = compiler.compileTemplate({
          source: descriptor.template.content,
          filename,
          id: filename,
        });

        snippets.push({
          code: compiled.code.replace(/_ctx\./g, ''),
          filename,
          line: descriptor.template.loc.start.line + 1,
        });
      }

      const scripts = [];

      if (descriptor.script && descriptor.script.content) {
        scripts.push(descriptor.script)
      }
      if (descriptor.scriptSetup && descriptor.scriptSetup.content) {
        scripts.push(descriptor.scriptSetup)
      }

      scripts.forEach((script) => {
        const scriptSnippet = {
          code: script.content,
          filename,
          line: script.loc.start.line,
        };

        if (
          snippets.length &&
          script.loc.start.line < descriptor.template.loc.start.line
        ) {
          snippets.unshift(scriptSnippet);
        } else {
          snippets.push(scriptSnippet);
        }
      })

      return resolve(snippets);
    });
  });
};

module.exports = extractVue3Snippets;

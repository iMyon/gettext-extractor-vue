const { CatalogBuilder } = require('gettext-extractor/dist/builder');

module.exports = class TransformLineCatalogBuilder extends CatalogBuilder {
  constructor(builderInstance, fn) {
    super(builderInstance.stats);
    this.transformator = fn;
    for (const prop in builderInstance.contexts) {
      if (builderInstance.contexts.hasOwnProperty(prop)) {
        throw new Error(
          'Make sure you run `addMessageTransformFunction` before extracting any strings'
        );
      }
    }
  }

  addMessage(message) {
    if (message.text) {
      message.text = this.transformator(message.text);
    }
    if (message.textPlural) {
      message.textPlural = this.transformator(message.textPlural);
    }
    super.addMessage(message);
  }
};

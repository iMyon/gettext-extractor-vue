const fs = require('fs');

const extractJSSnippets = (filename) => {
  const snippets = [];
  snippets.push({
    code: fs.readFileSync(filename, 'utf8'),
    filename,
    line: 1,
  });

  return Promise.resolve(snippets);
};

module.exports = extractJSSnippets;

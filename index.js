const decorateJSParserWithVueSupport = require('./lib/decorateJSParserWithVueSupport');
const decorateExtractorWithHelpers = require('./lib/decorateExtractorWithHelpers');

module.exports = {
  decorateJSParserWithVueSupport,
  decorateExtractorWithHelpers,
};
